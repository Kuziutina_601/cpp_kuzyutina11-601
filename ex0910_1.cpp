struct IntArray {
	int & get (size_t i) {
		return data_[i];
	}

	~IntArray() {
		delete[] data_;
	}



	// IntArray(int size = 0): size(size){}

	IntArray (IntArray const &a) :
		size_(a.size_), data_(new int[size_]) {
			for (int i = 0; i != size_; ++i)
			{
				data_[i] = a.data_[i];
			}
		}

	IntArray & operator=(IntArray const &a) {
		if(this != &a) {
			delete[] data_;
			size_ = a.size_;
			data_ = new int[size_];
			for (int i = 0; i != size_; ++i)
			{
				data_[i] = a.data_[i];
			}
			return * this;
		}
	}

	void swap(IntArray &a) {
		IntArray b = this;
		this = a;
		a = b;
	}



	int size_;
	// size_t a;
	// size_t b;
	int * data_;	

	// void resize(size_t nsize) {
	// 	int* ndata = new int[nsize];
	// 	//size_t///////
	// }
};
