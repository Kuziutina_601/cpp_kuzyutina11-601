#include <iostream>
#include <cstdlib>
using namespace std;

int ** create_array2d(size_t a, size_t b) {
	int ** m = new int * [a];
	for (size_t i = 0; i != a; ++i) {
		m[i] = new int[b];
	}
	return m;
}


int ** transpose(int ** m, unsigned rows, unsigned cols) {
	int ** mt = create_array2d(cols, rows);
	for (int i = 0; i < cols; ++i) {
		for (int j = 0; j < rows; ++j)
		{
			mt[i][j] = m[j][i];
		}
	}	
	return mt;

}

void print(const int* const * m, unsigned rows, unsigned cols) {
	for (int i = 0; i < rows; ++i) {
		for (int j = 0; j < cols; ++j)
		{
			cout << m[i][j] << "  ";
		}
		cout << endl;
	}
	cout << endl;
}

void fol(int** m, unsigned rows, unsigned cols) {
	int k = 0;
	for (int i = 0; i < rows; ++i) {
		for (int j = 0; j < cols; ++j)
		{
			m[i][j] = k;
			k++;
		}
	}
}

int main() {
	int ** m = create_array2d(4, 4);
	cout << "i create" << endl;
	fol(m, 4, 4);
	cout << "i fol" << endl;
	print(m, 4, 4);
	int ** mt = transpose(m, 4, 4);
	print(mt, 4, 4);

	return 0;
}