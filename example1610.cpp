#include <iostream>
#include <cstring>

using namespace std;

struct Person {

	Person(string name, int age):
		_name(name), _age(age)
		{}

	string name() const {return _name;}
	int age() const {return _age;}
protected:
	string _name;
	int _age;
};

struct Student:Person{
	Student(string name, int age, string uni):
		Person(name, age), _uni(uni)
		{}
	string uni() const {return _uni;}
private:
	string _uni;
};

int main() {
	return 0;
}

struct Vector2D
{
	Vector2D (double x, double y) : x(x), y(y){}
	Vector2D mult (double d) const {
		return Vector2D(x*d, y*d);
	}

	double mult(Vector2D const &p) const {
		return x*p.x + y*p.y;
	}

	double x, y;
};

int main() {
	Vector2D t(1, 2);-
	Vector2D m = t.mult(4);
	double r = t.mult(m);
}