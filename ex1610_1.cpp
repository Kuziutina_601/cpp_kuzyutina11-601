#include <iostream>

using namespace std;

struct Foo
{
	void say() const {
		std::cout<<"Foo says "<< msg << "\n";
	}
protected:
	Foo(const char * msg):msg(msg){}
private:
	const char * msg;
};

struct FooInh:Foo
{
	FooInh(const char * msg):Foo(msg){}
};

Foo getFoo(const char * msg) {

	FooInh fh(msg);
	Foo &m = fh;

	return m;
}

void foo_says(const Foo& foo) {
	foo.say();
}

int main() {


	foo_says(getFoo("1234"));

	return 0;
}
