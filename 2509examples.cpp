int ** create_array2d(size_t a, size_t b) {
	int ** m = new int * [a];
	for (size_t i = 0; i != a; ++i) {
		m[i] = new int[b];
	}
}

void  free_array2d(int ** m, size_t a, size_t b) {
	for (size_t i = 0; i != a; ++i)
	{
		delete[] m[i];
	}
	delete[] m;
}

int ** create_array2d(size_t a, size_t b) {
	int ** m = new int * [a];
	m[0] = new int[a * b];
	for (size_t i = 1; i != a; ++i) {
		m[i] = m[i-1] + b;
	}
}

void  free_array2d(int ** m, size_t a, size_t b) {
	delete[] m[0];
	delete[] m;
}



#include <string>
#include <fstream>

using namespace std;

int main() {
	string name;
	ifstream  input("input.txt");
	input >> name;
	ofstream output("output.txt");
	output << "hi, " << name << endl;
	input.close();
	return 0;
}


struct Point {
	double x;
	double y;
}

struct Segment{
	Point p1;
	Point p2;
	double length() {
		double dx = p1.x - p2.x;
		double dy = p1.y - p2.y;
		return sqrt(dx*dx + dy*dy);
	}	
}

#include <cmath>
double length(Segment s) {
	double dx = s.p1.x - s.p2.x;
	double dy = s.p1.y - s.p2.y;
	return sqrt(dx*dx + dy*dy);
}

double length(Segment *s) {
	double dx = s->p1.x - s->p2.x;
	double dy = s->p1.y - s->p2.y;
	return sqrt(dx*dx + dy*dy);
}

struct intArray2D {
	int & get (size_t i, size_t j) {
		return data[i*b + j];
	}

	~intArray2D() {
		delete[] data;
	}

	intArray2D(int size = 0): size(size){}

	int size;
	size_t a;
	size_t b;
	int ** data;	
};

intArray2D k = {n, m, create_array2d(n, m)};