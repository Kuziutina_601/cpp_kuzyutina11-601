#include <iostream>
#include <string>

using namespace std;

struct Person
{
	Person(string , int age):
		name_(name),age_(age){};

	string name() const {
		return name_;
	}

	virtual string occupation() const = 0;

	virtual ~Person(){};

	string name_;
	int age_;
};

struct Professor:Person 
{
	Professor(string name): Person(name){};
	string name() const{
		return "Professor" + name_;
	}
};

struct Student
{
	Student(string name, int age, string uni):
	Person(name, age), uni_(uni){};

};

struct IBase
{
	virtual void foo(int n=1) const = 0;
	virtual ~IBase() = 0;
};

void IBase::foo(int n) const {cout<<n << "foo\n";}

IBase::~IBase() { cout << "ddd";}

struct Derived:IBase
{
	virtual void foo(int n = 2) const {IBase::foo(n);}
};

void bar(const IBase &arg) {arg.foo();}


int main() {
	// Professor pr("Stro");
	// cout << pr.name() << endl;
	// Person *p = &pr;
	// cout << p->name() << endl;
	bar(Derived());
	return 0;
}