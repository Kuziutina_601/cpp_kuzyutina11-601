#include <iostream>

using namespace std;


char* resize(char *str, unsigned size, unsigned new_size) {
	char * m = new char[new_size];
	
	for (int i = 0; i < size; i++) {
		m[i] = str[i];
	}
	delete [] str;
	// cout << m << endl;
	return m;
}

char* getline() {
	char k;
	cin >> k;
	char* m = new char[1];
	int size = 0;

	while (k != '\0' && k != '\n' /*&& k!='a'*/) {   //для теста с консоли лучше использовать определенный символ, например 'a'
		m = resize(m, size, size+1);
		size++;
		m[size-1] = k;
		cin >> k;
	}
	m = resize(m, size, size+1);
	m[size] = '\0';
	
	
	return m;

}


int main() {
	char* k = getline();
	cout << "result :  " << k;

	// coll();

    // int a[5] = {1, 2, 3, 4, 5};
    
    // rotate(a, 5, 2);

    // int m[3][3] = {{1,2 ,3}, {4, 5, 6} , {7, 8, 9}};
    // top_max(m, 3, 3);

	return 0;
}
