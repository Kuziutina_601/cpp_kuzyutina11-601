#include <iostream>
#include <string>

using namespace std;

struct Expression {
	virtual double evaluate() const = 0;

	virtual ~Expression() {}
};

struct Number:Expression {
	Number(double value): value_(value){}

	double evaluate() const{
		return value_;
	}
	
private:
	double value_;
};

bool check_equals(Expression const *left, Expression const*right) {
	// return *(int **)left = *(int**)right;
	return typeid(left) == typeid(right);
}

struct BinaryOperation:Expression {
	BinaryOperation(Expression const *left,
		char op, Expression const *right):left(left),op(op),right(right){}

	double evaluate() const {
		if (op == '+') {
			return (*right).evaluate() + (*left).evaluate();
		}
		if (op == '*') {
			return (*right).evaluate() * (*left).evaluate();
		}
		return 0;
	}

	~BinaryOperation(){
		delete left;
		delete right;
	}
private:
	Expression const *left;
	Expression const *right;
	char op;

};



int main() {
	Expression *sube = new BinaryOperation(new Number(4.5), '*', new Number(5));
	Expression *expr = new BinaryOperation(new Number(3), '+', sube);
	cout << expr->evaluate() << endl;
	delete expr;
}