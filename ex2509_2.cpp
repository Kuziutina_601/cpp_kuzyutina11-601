#include <iostream>
#include <cmath>

using namespace std;

struct Point {
	double x;
	double y;

    Point(double x = 0, double y = 0) : x(x), y(y) //это с параметром по умолчанию
	{} //они идентичны с закоменченными

	// Point() {x = y = 0;}
	// Point(double x, double y) {
	// 	this->x = x;
	// 	this->y = y;
	// }
	void shift(double x, double y) {
		this -> x += x;
		this -> y += y;
	}
};

struct Segment{
	Point p1;
	Point p2;
	Segment() {}
	Segment(double length):p2(length, 0) {}
	Segment(Point p1, Point p2):p1(p1),p2(p2) {}
	double length() {
		double dx = p1.x - p2.x;
		double dy = p1.y - p2.y;
		return sqrt(dx*dx + dy*dy);
	}
};

int main() {

	Segment s3 = 20;
	return 0;
}
