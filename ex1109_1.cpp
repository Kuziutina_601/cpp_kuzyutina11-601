#include <iostream>

using namespace std;

#define max(a,b,c) int k = (a); int u = (b); (c) = k < u ? u : k;

int main() 
{
	int a = 10;
	int b = 20;
	int c = 0;

	max(++a, b--, c);

	cout << c;


	return 0;
}